package cn.qnkj.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;


/**
 * @author: zds
 * Date: 2020/5/18  19:59
 * Version: V 1.0
 * Description:
 */
@EnableEurekaServer
@SpringBootApplication
public class EurakaServer {
    public static void main(String[] args){
        SpringApplication.run(EurakaServer.class);
    }
}
