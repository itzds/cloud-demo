package com.qnkj.user.service;

import com.qnkj.user.mapper.UserMapper;
import com.qnkj.user.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: zds
 * Date: 2020/5/12  20:59
 * Version: V 1.0
 * Description:
 */
@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public User queryByid(Long id){
        return userMapper.selectByPrimaryKey(id);
    }
}

