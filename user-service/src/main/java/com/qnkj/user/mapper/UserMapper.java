package com.qnkj.user.mapper;

import com.qnkj.user.pojo.User;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {
}
