package com.qnkj;

import com.qnkj.user.pojo.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author: zds
 * Date: 2020/5/12  20:42
 * Version: V 1.0
 * Description:
 */
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan("com.qnkj.user.mapper")
public class UserApplication {
    public static void main(String[] aggs){
        SpringApplication.run(UserApplication.class);
    }
}
