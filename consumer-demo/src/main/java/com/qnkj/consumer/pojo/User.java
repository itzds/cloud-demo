package com.qnkj.consumer.pojo;

import lombok.Data;
import java.util.Date;

/**
 * @author: zds
 * Date: 2020/5/12  20:46
 * Version: V 1.0
 * Description:
 */
@Data
public class User {

    private Long id;
    private String userName;
    private String password;
    private String name;
    private Integer age;
    private Integer sex;
    private Date birthDay;
    private Date created;
    private Date updated;
    private String note;
}
