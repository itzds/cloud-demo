package com.qnkj.consumer.client;

import org.springframework.stereotype.Component;

/**
 * @author: zds
 * Date: 2020/5/31  19:10
 * Version: V 1.0
 * Description: 熔断处理
 */
@Component
public class UserClientFallBack implements UserClient {
    @Override
    public String queryById(Long id) {
        return "查询失败！";
    }

    @Override
    public String queryByName(String name) {
        return "查询失败！";
    }
}
