package com.qnkj.consumer.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "user-service",fallback = UserClientFallBack.class)
public interface UserClient {

    @GetMapping("/user/{id}")
    String queryById(@PathVariable("id") Long id);

    @GetMapping("/user/{name}")
    String queryByName(@PathVariable("name") String name);
}
