package com.qnkj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @author: zds
 * Date: 2020/5/12  21:19
 * Version: V 1.0
 * Description:
 */

//@EnableCircuitBreaker
//@EnableDiscoveryClient
//@SpringBootApplication  这三个注解简化为一个 @SpringCloudApplication

@SpringCloudApplication
@EnableFeignClients(basePackages = {"com.qnkj.consumer.client"})
public class ConsumerApplication {

    /*@Bean
    @LoadBalanced
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }*/
    public static void main(String[] args){
        SpringApplication.run(ConsumerApplication.class);
    }
}
